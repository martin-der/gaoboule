package net.tetrakoopa.gaoboule;


import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Runner {

	private ScriptEngineManager manager = new ScriptEngineManager();
	private ScriptEngine engine = manager.getEngineByExtension("js");


	void test() {
		try {

			engine.eval("function afficher(valeur) {"
					+ "var sortie = '';"
					+ "sortie = 'chaine' + valeur;"
					+ "return sortie;"
					+ "}");

			if (engine instanceof Invocable) {
				Invocable invocableEngine = (Invocable) engine;
				Object resultat = invocableEngine.invokeFunction("afficher",
						10);
				System.out.println("resultat = " + resultat);
			} else {
				System.err.println("Le moteur n'implemente pas l'interface Invocable");
			}

		} catch (ScriptException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

	}
}
