package net.tetrakoopa.gaoboule.model.misc;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import net.tetrakoopa.gaoboule.config.common.OneOrManyStringDeserializer;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Debug {

	@Getter
	@Setter
	public static class Behavior {
		@JsonDeserialize(using = OneOrManyStringDeserializer.class)
		private List<String> react = new ArrayList<>();
	}

	private Log log;

	private Behavior acknowledge;
	private Behavior denied;
	private Behavior invalid;
}
