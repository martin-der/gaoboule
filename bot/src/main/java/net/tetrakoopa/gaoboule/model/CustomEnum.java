package net.tetrakoopa.gaoboule.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomEnum {

	private final List<String> values = new ArrayList<>();
}
