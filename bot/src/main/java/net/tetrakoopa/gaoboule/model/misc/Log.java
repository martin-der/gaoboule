package net.tetrakoopa.gaoboule.model.misc;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Log {

	private String room;

	private final Map<String, String> levels = new HashMap<>();
}
