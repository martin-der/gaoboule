package net.tetrakoopa.gaoboule.model.board;

public class DefaultProcessor implements Processor {

	private final BUS bus;

	private final Composition composition;

	public DefaultProcessor(Composition composition, BUS bus) {
		this.composition = composition;
		this.bus = bus;
	}

	@Override
	public <E extends Event<E>> void handle(Event.Instance<E> event) {
		
		if (event instanceof Event.HttpRequest.Instance) {
			handleHttpRequest((Event.HttpRequest.Instance) event);
			return;
		}

		if (event instanceof Event.Matrix.Message.Instance) {
			handleMatrixMessage((Event.Matrix.Message.Instance) event);
			return;
		}

		if (event instanceof Event.Matrix.Command.Instance) {
			handleMatrixCommand((Event.Matrix.Command.Instance) event);
			return;
		}

		if (event instanceof Event.Matrix.Reaction.Instance) {
			handleMatrixReaction((Event.Matrix.Reaction.Instance) event);
			return;
		}

	}

	private void handleHttpRequest(Event.HttpRequest.Instance event) {
		final Event.HttpRequest prototype = event.getPrototype();
	}

	private void handleMatrixMessage(Event.Matrix.Message.Instance event) {
		final Event.Matrix.Message prototype = event.getPrototype();
	}

	private void handleMatrixCommand(Event.Matrix.Command.Instance event) {
		final Event.Matrix.Command prototype = event.getPrototype();
	}

	private void handleMatrixReaction(Event.Matrix.Reaction.Instance event) {
		final Event.Matrix.Reaction prototype = event.getPrototype();
	}

}
