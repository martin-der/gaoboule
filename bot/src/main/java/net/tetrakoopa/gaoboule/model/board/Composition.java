package net.tetrakoopa.gaoboule.model.board;

import net.tetrakoopa.gaoboule.model.Project;
import net.tetrakoopa.gaoboule.model.Room;
import net.tetrakoopa.gaoboule.model.User;
import net.tetrakoopa.gaoboule.model.misc.Debug;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

@Getter
public class Composition {

	private final Map<String, Action> actions = new HashMap<>();

	private final List<Event> events = new ArrayList<>();

	private final List<Actuator> actuators = new ArrayList<>();

	private final Map<String, Room> rooms = new HashMap<>();

	private final Map<String, User> users = new HashMap<>();

	private final Map<String, Project> projects = new HashMap<>();

	private final List<String> rights = new ArrayList<>();

	private Debug debug;
}
