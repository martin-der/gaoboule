package net.tetrakoopa.gaoboule.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import net.tetrakoopa.gaoboule.config.RoomDeserializer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonDeserialize(using = RoomDeserializer.class)
public class Room {

	private String mxid;
}
