package net.tetrakoopa.gaoboule.model.board;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import net.tetrakoopa.gaoboule.config.EventDeserializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonDeserialize(using = EventDeserializer.class)
public abstract class Event<E extends Event<E>> {

	public interface DefinitionValues extends Map<String, String> {
		class Builder {
			public static class Default extends HashMap<String, String> implements DefinitionValues {}
			public static DefinitionValues from(Map<String, String> values) {
				final DefinitionValues definitionValues = new Default();
				definitionValues.putAll(values);
				return definitionValues;
			}
		}
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.TYPE})
	public @interface Definition {
		@FunctionalInterface
		interface DefinitionValuesMatcher {
			boolean match(DefinitionValues definitionValues);
		}
		Class<? extends DefinitionValuesMatcher> matcher();

		String identifier();
	}

	public enum Type {
		WEB_REQUEST,
		MATRIX_MESSAGE_REGEX,
		MATRIX_COMMAND,
		MATRIX_REACTION
	}
	

	private String action;

	protected abstract Instance<E> buildInstance();

	@Getter
	@Setter
	@Definition(matcher = HttpRequest.Matcher.class, identifier = "http-request")
	public static class HttpRequest extends Event<HttpRequest> {

		public static final String ATTRIBUTE_NAME_PATH = "web-request-path";

		private final Set<String> methods =  new HashSet<>();

		private String path;

		@Getter
		@Setter
		public static class Instance extends Event.Instance<HttpRequest> {

			private String url;

			private String method;

			private final Map<String, String> parameters = new HashMap<>();

			private final Map<String, String> pathVariables = new HashMap<>();

			protected Instance(HttpRequest event) {
				super(event);
			}
		}

		@Override
		protected Event.Instance<HttpRequest> buildInstance() {
			return new Instance(this);
		}

		public static class Matcher implements Definition.DefinitionValuesMatcher {
			@Override
			public boolean match(DefinitionValues definitionValues) {
				if (!definitionValues.containsKey(ATTRIBUTE_NAME_PATH)) {
					return false;
				}
				return true;
			}
		}
	}

	@Getter
	@Setter
	@Definition(matcher = Message.Matcher.class, identifier = "matrix-message")
	public static class Message extends Matrix<Message> {

		public static final String ATTRIBUTE_NAME_REGEX = "matrix-message";

		private String messageMatcher;

		public static class Instance extends Event.Instance<Message> {
			protected Instance(Message event) {
				super(event);
			}
		}

		@Override
		protected Event.Instance<Message> buildInstance() {
			return new Instance(this);
		}

		public static class Matcher implements Definition.DefinitionValuesMatcher {
			@Override
			public boolean match(DefinitionValues definitionValues) {
				if (!definitionValues.containsKey(ATTRIBUTE_NAME_REGEX)) {
					return false;
				}
				return true;
			}
		}
	}

	@Getter
	@Setter
	public static abstract class Matrix<M extends Matrix<M>> extends Event<M> {

		private String rawContent;

		@Definition(matcher = Command.Matcher.class, identifier = "matrix-command")
		public static class Command extends Matrix<Command> {

			public static final String ATTRIBUTE_NAME_COMMAND = "command";

			@Getter
			@Setter
			private String command;

			@Getter
			@Setter
			private final List<String> parameters = new ArrayList<>();

			public static class Instance extends Event.Instance<Command> {
				protected Instance(Command event) {
					super(event);
				}
			}

			@Override
			protected Event.Instance<Command> buildInstance() {
				return new Instance(this);
			}

			public static class Matcher implements Definition.DefinitionValuesMatcher {
				@Override
				public boolean match(DefinitionValues definitionValues) {
					if (!definitionValues.containsKey(ATTRIBUTE_NAME_COMMAND)) {
						return false;
					}
					return true;
				}
			}
		}

		@Getter
		@Setter
		@Definition(matcher = Reaction.Matcher.class, identifier = "matrix-reaction")
		public static class Reaction extends Matrix<Reaction> {

			public static final String ATTRIBUTE_NAME_REACTION = "reaction";

			private String reaction;

			public static class Instance extends Event.Instance<Reaction> {
				protected Instance(Reaction event) {
					super(event);
				}
			}

			@Override
			protected Event.Instance<Reaction> buildInstance() {
				return new Instance(this);
			}

			public static class Matcher implements Definition.DefinitionValuesMatcher {
				@Override
				public boolean match(DefinitionValues definitionValues) {
					if (!definitionValues.containsKey(ATTRIBUTE_NAME_REACTION)) {
						return false;
					}
					return true;
				}
			}
		}
	}

	@Getter
	@Setter
	public static abstract class Instance<E extends Event> {
		private final E prototype;

		protected Instance(E prototype) {
			this.prototype = prototype;
		}
	}

	public final Instance<E> instance() {
		return buildInstance();
	}

}
