package net.tetrakoopa.gaoboule.model.board;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import net.tetrakoopa.gaoboule.config.EventTypeDeserializer;
import net.tetrakoopa.gaoboule.config.InjectorDeserializer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Actuator {

	@Getter
	@Setter
	@JsonDeserialize(using = InjectorDeserializer.class)
	public static class Injector {

		@Getter
		@Setter
		public static class ExpressionToNameMapper {
			private String source;
			private String target;
		}

		private final List<ExpressionToNameMapper> mappers = new ArrayList<>();
	}

	private String action;

	@JsonDeserialize(keyUsing = EventTypeDeserializer.class)
	private final Map<Event.Type, Injector> injectors = new HashMap<>();

	private final List<String> requisites = new ArrayList<>();

}
