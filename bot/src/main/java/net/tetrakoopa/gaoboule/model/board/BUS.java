package net.tetrakoopa.gaoboule.model.board;

import net.tetrakoopa.gaoboule.model.Room;

import java.util.Optional;

public class BUS {

	private final Composition composition;

	public BUS(Composition composition) {
		this.composition = composition;
	}

	public <E> Optional<E> resolve(Class<E> clazz, String id) {
		if (clazz.equals(Room.class)) {
			return composition.getRooms().containsKey(id) ? (Optional<E>)Optional.of(composition.getRooms().get(id)) : Optional.empty();
		}
		throw new IllegalArgumentException("Don't know how to resolve "+clazz.getName());
	}
}
