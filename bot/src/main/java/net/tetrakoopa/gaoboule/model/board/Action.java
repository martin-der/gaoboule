package net.tetrakoopa.gaoboule.model.board;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import net.tetrakoopa.gaoboule.config.ActionCommandDeserializer;
import net.tetrakoopa.gaoboule.config.ActionParameterDeserializer;
import net.tetrakoopa.gaoboule.config.ActionValueAsMapValueDeserializer;
import net.tetrakoopa.gaoboule.config.ActionValueDeserializer;
import net.tetrakoopa.gaoboule.config.ActionValueNameAsMapKeyDeserializer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Action {

	@Getter
	@Setter
	@JsonDeserialize(using = ActionParameterDeserializer.class)
	public static class Parameter {
		public enum Type {
			STRING, INTEGER, FLOAT, BOOLEAN, OBJECT
		}
		public enum Mode {
			EVALUATION, EXECUTION, STRING_FORMAT
		}

		private String name;

		private Type type;

		private boolean mandatory;

		private String automaticContext;
	}

	@Getter
	@Setter
	@JsonDeserialize(using = ActionValueDeserializer.class)
	public static class Value {
		public enum Mode {
			EVALUATION, EXECUTION, STRING_FORMAT
		}

		private Mode mode;

		private String expression;

	}

	@JsonDeserialize(using = ActionCommandDeserializer.class)
	@Getter
	@Setter
	public static abstract class Command {

		private String commandType;

		protected abstract boolean accept(Map<String, Class<?>> attributes);

		@Getter
		@Setter
		public static class HttpRequest extends Command {
			private String url;
			private String method;

			@Override
			protected boolean accept(Map<String, Class<?>> attributes) {
				return false;
			}
		}
		@Getter
		@Setter
		public static class MatrixRoomMessage extends Command {

			@Override
			protected boolean accept(Map<String, Class<?>> attributes) {
				return false;
			}
		}

	}

	private String name;

	@JsonBackReference()
	private final Map<String, Parameter> parameters = new HashMap<>();

	@JsonDeserialize(keyUsing = ActionValueNameAsMapKeyDeserializer.class, contentUsing = ActionValueAsMapValueDeserializer.class)
	private final Map<String, Value> values = new HashMap<>();

	private final List<Command> commands = new ArrayList<>();
	@JsonProperty("command")
	public final void setCommand(Command command) {
		commands.add(command);
	}

	@JsonIgnore
	private final List<String> requirements = new ArrayList<>();
}
