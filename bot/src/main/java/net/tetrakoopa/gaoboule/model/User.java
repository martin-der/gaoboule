package net.tetrakoopa.gaoboule.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

	private String mxid;

	private final List<String> rights = new ArrayList<>();
}
