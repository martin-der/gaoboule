package net.tetrakoopa.gaoboule.model.board;

public interface Processor {

	<E extends Event<E>> void handle(Event.Instance<E> event);

}
