package net.tetrakoopa.gaoboule.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Identified {

	private String id;
}
