package net.tetrakoopa.gaoboule.identity;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
public class Identity {

	@Getter
	@Setter
	public static class Matrix {
		private String domain;
		@JsonAlias({"localpart", "local-part"})
		private String localPart;
		private String token;
		private String password;
	}

	private Matrix matrix;

}
