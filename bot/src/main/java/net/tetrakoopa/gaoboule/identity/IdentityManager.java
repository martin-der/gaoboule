package net.tetrakoopa.gaoboule.identity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.io.InputStream;

public class IdentityManager {

	private final ObjectMapper mapper;

	public IdentityManager() {
		this.mapper = new ObjectMapper(new YAMLFactory());
	}

	public Identity load(InputStream input) throws IOException {
		return mapper.readValue(input, Identity.class);
	}
}
