package net.tetrakoopa.gaoboule;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ExecutionSandbox {

	private final ScriptEngineManager manager;
	private final ScriptEngine engine;

	public ExecutionSandbox() {
		manager = new ScriptEngineManager();
		engine = manager.getEngineByName("graal.js");
	}

	public Instance instance() {
		return new Instance();
	}

	public class Instance {

		public void execute() throws ScriptException, NoSuchMethodException {

			engine.eval("function afficher(valeur) {"
					+ "var sortie = '';"
					+ "sortie = 'chaine' + valeur;"
					+ "return sortie;"
					+ "}");

			Invocable invocableEngine = (Invocable) engine;
			Object resultat = invocableEngine.invokeFunction("afficher",
					10);
			System.out.println("resultat = " + resultat);
		}
	}

}
