package net.tetrakoopa.gaoboule.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	public static List<String> splitOnSpaceExceptedQuoteEscaped(String string) {
		final List<String> parts = new ArrayList<String>();
		final Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
		final Matcher regexMatcher = regex.matcher(string);
		while (regexMatcher.find()) {
			if (regexMatcher.group(1) != null) {
				// Add double-quoted string without the quotes
				parts.add(regexMatcher.group(1));
			} else if (regexMatcher.group(2) != null) {
				// Add single-quoted string without the quotes
				parts.add(regexMatcher.group(2));
			} else {
				// Add unquoted word
				parts.add(regexMatcher.group());
			}
		}
		return parts;
	}

}
