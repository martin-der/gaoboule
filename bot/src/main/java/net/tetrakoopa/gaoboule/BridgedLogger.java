package net.tetrakoopa.gaoboule;

import net.tetrakoopa.gaoboule.model.Room;
import net.tetrakoopa.gaoboule.model.board.BUS;
import net.tetrakoopa.gaoboule.model.board.Composition;

import java.util.Collections;
import java.util.Map;

import io.github.ma1uta.matrix.client.StandaloneClient;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic="gaoboule-bot")
public class BridgedLogger implements Logger {

	private final Room logRoom;

	private final StandaloneClient mxClient;

	private final Map<String, String> prefixes;

	public enum Severity {
		CRITICAL,
		ERROR,
		WARNING,
		INFO,
		SUCCESS,
		DEBUG
	}

	public BridgedLogger(Composition composition, BUS bus, StandaloneClient mxClient) {

		this.mxClient = mxClient;

		Room room = null;
		Map<String, String> prefixes = null;

		if (composition.getDebug() != null) {
			if (composition.getDebug().getLog() != null) {
				if (composition.getDebug().getLog().getRoom() != null) {
					room = bus.resolve(Room.class, composition.getDebug().getLog().getRoom()).orElse(null);
				}
				prefixes = composition.getDebug().getLog().getLevels();
			}
		}

		this.prefixes = prefixes == null ? Collections.emptyMap() : prefixes;
		this.logRoom = room;

	}

	protected void log(String message, Severity severity) {
		if (logRoom != null) {
			if (prefixes.containsKey(severity.name())) {
				message = prefixes.get(severity.name()) + message;
			}
			mxClient.event().sendFormattedNotice(logRoom.getMxid(), message, message);
		}
	}


	@Override
	public void critical(String text) {
		log(text, Severity.CRITICAL);
	}

	@Override
	public void error(String text) {
		log(text, Severity.ERROR);
	}

	@Override
	public void warning(String text) {
		log(text, Severity.WARNING);
	}

	@Override
	public void info(String text) {
		log(text, Severity.INFO);
	}

	@Override
	public void success(String text) {
		log(text, Severity.SUCCESS);
	}

	@Override
	public void debug(String text) {
		log(text, Severity.DEBUG);
	}

}
