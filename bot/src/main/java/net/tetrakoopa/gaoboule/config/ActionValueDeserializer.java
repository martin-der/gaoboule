package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import net.tetrakoopa.gaoboule.model.board.Action;

import java.io.IOException;

public class ActionValueDeserializer extends AbstractDeserializer<Action.Value> {

	@Override
	public Action.Value deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final JsonNode node = parser.getCodec().readTree(parser);
		final Action.Value value = new Action.Value();
		final String expression = node.asText();
		value.setExpression(expression);
		return value;
	}
}
