package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import net.tetrakoopa.gaoboule.model.board.Composition;

import java.io.IOException;
import java.io.InputStream;

public class ConfigManager {

	private final ObjectMapper mapper;

	public ConfigManager() {
		this.mapper = new ObjectMapper(new YAMLFactory());
	}

	public Composition load(InputStream input) throws IOException {
		return mapper.readValue(input, Composition.class);
	}
}
