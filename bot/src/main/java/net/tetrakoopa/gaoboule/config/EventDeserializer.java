package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import net.tetrakoopa.gaoboule.model.board.Event;
import net.tetrakoopa.gaoboule.util.StringUtil;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EventDeserializer extends AbstractDeserializer<Event<?>> {

	private final static String NODE_NAME_ACTION = "action";

	private final static List<EventHelper> events = new ArrayList<>();
	public static void addEvent(Class<? extends Event<?>> eventClass) {
		final Event.Definition definition = eventClass.getAnnotation(Event.Definition.class);
		try {
			events.add(new EventHelper(eventClass, definition.identifier(), definition.matcher().getDeclaredConstructor().newInstance()));
		} catch (InstantiationException|IllegalAccessException|InvocationTargetException|NoSuchMethodException e) {
			throw new IllegalStateException("Failed to create event matcher '"+eventClass.getName()+"' : "+e.getMessage(), e);
		}
	}
	private final static class EventHelper {
		final String typeName;
		final Event.Definition.DefinitionValuesMatcher matcher;
		final Class<? extends Event<?>> eventClass;

		private EventHelper(Class<? extends Event<?>> eventClass, String typeName, Event.Definition.DefinitionValuesMatcher matcher) {
			this.eventClass = eventClass;
			this.typeName = typeName;
			this.matcher = matcher;
		}
	}
	static {
		addEvent(Event.HttpRequest.class);
		addEvent(Event.Matrix.Command.class);
		addEvent(Event.Matrix.Reaction.class);
		addEvent(Event.Matrix.Message.class);
	}

	@Override
	public Event<?> deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final JsonNode node = parser.getCodec().readTree(parser);

		final Map<String, String> keys = new HashMap<>();
		node.fields().forEachRemaining(f -> {
			if (!f.getKey().equals(NODE_NAME_ACTION)) {
				keys.put(f.getKey(), f.getValue().getNodeType().name());
			}
		});
		final Event.DefinitionValues definitionValues = Event.DefinitionValues.Builder.from(keys);

		final Map<String, Class<? extends Event<?>>> possibleEvents =
		events.stream()
				.filter(eventHelper -> eventHelper.matcher.match(definitionValues))
				.collect(Collectors.toMap(eventHelper -> eventHelper.typeName, eventHelper -> eventHelper.eventClass));

		if (possibleEvents.isEmpty()) {
			throw new IllegalStateException("Could not guess matching event, please provide a type name from "
					+ events.stream().map(e -> e.typeName).collect(Collectors.joining(", ")));
		}
		if (possibleEvents.size()>1) {
			throw new IllegalStateException("Multiple events could match, please provide a type name from "
					+ possibleEvents.keySet().stream().collect(Collectors.joining(", ")));
		}

		final Map.Entry<String, Class<? extends Event<?>>> possibleEvent = possibleEvents.entrySet().iterator().next();
		final Event event;
		try {
			event = possibleEvents.entrySet().iterator().next().getValue().getDeclaredConstructor().newInstance();
		} catch (InstantiationException|IllegalAccessException|InvocationTargetException|NoSuchMethodException e) {
			throw new IllegalStateException("Failed to create event '"+possibleEvent.getKey()+"' : "+e.getMessage(), e);
		}

		final JsonNode actionNode = node.get("action");
		if (actionNode == null) {
			throw new RuntimeException("action required");
		}
		event.setAction(actionNode.asText());

		if (event instanceof Event.HttpRequest) {
			final Event.HttpRequest httpRequestEvent = (Event.HttpRequest)event;
			httpRequestEvent.setPath(node.get("http-request-path").asText());
			if (node.has("mode")) {
				JsonNode modeNode = node.get("mode");
				if (modeNode.isTextual()) {
					httpRequestEvent.getMethods().add(modeNode.asText());
				} else if (modeNode.isArray()) {
					modeNode.elements().forEachRemaining(e -> httpRequestEvent.getMethods().add(modeNode.asText()));
				} else {
					throw new IllegalStateException("Don't known how to handle 'mode'");
				}
			}
		}
		else if (event instanceof Event.Matrix.Command) {
			final Event.Matrix.Command commandEvent = (Event.Matrix.Command)event;
			JsonNode commandNode = node.get("command");
			if (commandNode.isTextual()) {
				final String rawCommand = commandNode.asText();
				final List<String> parts = StringUtil.splitOnSpaceExceptedQuoteEscaped(rawCommand);
				commandEvent.setCommand(parts.get(0));
//				commandEvent.;
			} else {
				throw new IllegalStateException("Don't known how to handle 'command'");
			}
//			commandEvent.setCommand();
		}

		return event;
	}
}
