package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import net.tetrakoopa.gaoboule.model.Room;

import java.io.IOException;

public class RoomDeserializer extends AbstractDeserializer<Room> {

	@Override
	public Room deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final JsonNode node = parser.getCodec().readTree(parser);
		final Room room = new Room();
		if (node.getNodeType() == JsonNodeType.STRING) {
			room.setMxid(node.asText());
		}
		return room;
	}
}
