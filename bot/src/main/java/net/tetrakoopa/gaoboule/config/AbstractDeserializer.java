package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public abstract class AbstractDeserializer<E> extends StdDeserializer<E> {

	protected AbstractDeserializer() {
		this(null);
	}

	protected AbstractDeserializer(Class<?> vc) {
		super(vc);
	}
}
