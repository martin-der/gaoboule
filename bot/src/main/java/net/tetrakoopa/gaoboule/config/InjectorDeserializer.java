package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import net.tetrakoopa.gaoboule.model.board.Actuator;

import java.io.IOException;

public class InjectorDeserializer extends AbstractDeserializer<Actuator.Injector> {

	@Override
	public Actuator.Injector deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final JsonNode node = parser.getCodec().readTree(parser);

		final Actuator.Injector injector = new Actuator.Injector();

		return injector;
	}
}
