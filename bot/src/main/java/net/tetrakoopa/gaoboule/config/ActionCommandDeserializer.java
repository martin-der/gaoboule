package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import net.tetrakoopa.gaoboule.model.board.Action;

import java.io.IOException;

public class ActionCommandDeserializer extends AbstractDeserializer<Action.Command> {

	@Override
	public Action.Command deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final JsonNode node = parser.getCodec().readTree(parser);
		final Action.Command command = new Action.Command.HttpRequest();
		return command;
	}
}
