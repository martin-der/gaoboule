package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

import net.tetrakoopa.gaoboule.model.board.Action;

import java.io.IOException;

public class ActionValueAsMapValueDeserializer extends AbstractDeserializer<Action.Value> {

	@Override
	public Action.Value deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final String name = context.getParser().getCurrentName();
		final Action.Value value = parser.readValueAs(Action.Value.class);
		if (name.endsWith("%")) {
			value.setMode(Action.Value.Mode.STRING_FORMAT);
 		} else if (name.endsWith("*")) {
			value.setMode(Action.Value.Mode.EXECUTION);
		} else {
			value.setMode(Action.Value.Mode.EVALUATION);
		}
		return value;
	}
}
