package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

import net.tetrakoopa.gaoboule.model.board.Event;

import java.io.IOException;
import java.util.Locale;

public class EventTypeDeserializer extends KeyDeserializer {

	@Override
	public Event.Type deserializeKey(
			String key,
			DeserializationContext context) throws IOException, JsonProcessingException {

		return Event.Type.valueOf(key.toUpperCase(Locale.ROOT).replace(".", "_"));
	}
}
