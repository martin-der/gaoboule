package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import net.tetrakoopa.gaoboule.model.board.Action;

import java.io.IOException;

public class ActionParameterDeserializer extends AbstractDeserializer<Action.Parameter> {

	@Override
	public Action.Parameter deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final JsonNode node = parser.getCodec().readTree(parser);
		final Action.Parameter parameter = new Action.Parameter();
		if (node.getNodeType() == JsonNodeType.STRING) {
			parameter.setType(getType(node.asText()));
		} else if (node.getNodeType() == JsonNodeType.OBJECT) {
			JsonNode attribute;
			attribute = node.get("type");
			if (attribute != null)
				parameter.setType(getType(attribute.asText()));
			attribute = node.get("mandatory");
			if (attribute != null)
				parameter.setMandatory(attribute.asBoolean());
			attribute = node.get("automatic");
			if (attribute != null)
				if (attribute.getNodeType() == JsonNodeType.BOOLEAN)
					parameter.setAutomaticContext(attribute.asBoolean()?"*": null);
				else
					parameter.setAutomaticContext(attribute.asText());
		}
		return parameter;
	}

	private Action.Parameter.Type getType(String value) {
		switch (value) {
			case "string":
				return Action.Parameter.Type.STRING;
			case "bool": case "boolean":
				return Action.Parameter.Type.BOOLEAN;
			case "float":
				return Action.Parameter.Type.FLOAT;
			case "integer":
				return Action.Parameter.Type.INTEGER;
		}

		return Action.Parameter.Type.OBJECT;
//		throw new IllegalArgumentException("No such Action.Parameter.Type with name "+value);
	}
}
