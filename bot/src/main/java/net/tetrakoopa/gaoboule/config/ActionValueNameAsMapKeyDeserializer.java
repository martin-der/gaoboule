package net.tetrakoopa.gaoboule.config;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

import java.io.IOException;

public class ActionValueNameAsMapKeyDeserializer extends KeyDeserializer {

	@Override
	public Object deserializeKey(String key, DeserializationContext context) throws IOException {
		if (key.endsWith("%") || key.endsWith("*")) {
			return key.substring(0, key.length()-1);
		}
		return key;
	}
}
