package net.tetrakoopa.gaoboule.config.common;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import net.tetrakoopa.gaoboule.config.AbstractDeserializer;
import net.tetrakoopa.gaoboule.model.misc.Debug;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OneOrManyStringDeserializer extends AbstractDeserializer<List<String>> {

	@Override
	public List<String> deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
		final JsonNode node = parser.getCodec().readTree(parser);
		final Debug.Behavior behavior = new Debug.Behavior();
		if (node.getNodeType() == JsonNodeType.STRING) {
			return Collections.singletonList(node.asText());
		}
		if (node.getNodeType() == JsonNodeType.ARRAY) {
			List<String> strings = new ArrayList<>();
			node.elements().forEachRemaining(e -> {
				strings.add(e.asText());
			});
			return strings;
		}
		throw new IllegalArgumentException("Not a valid String/Strings content");
	}

}
