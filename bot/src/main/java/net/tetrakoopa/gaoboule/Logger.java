package net.tetrakoopa.gaoboule;

public interface Logger {

	void critical(String text);
	void error(String text);
	void warning(String text);
	void info(String text);
	void success(String text);
	void debug(String text);

}
