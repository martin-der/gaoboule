package net.tetrakoopa.gaoboule;

import org.junit.jupiter.api.Test;

import javax.script.ScriptException;

public class ExecutionSandboxTest {

	static ExecutionSandbox sandbox = new ExecutionSandbox();

	@Test
	public void dummy() throws ScriptException, NoSuchMethodException {
		sandbox.instance().execute();
	}
}
