package net.tetrakoopa.gaoboule.test;

import net.tetrakoopa.gaoboule.util.StringUtil;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringUtilTest {

	@Test
	public void ttt() {
		List<String> parts;
		String[] expected;

		parts = StringUtil.splitOnSpaceExceptedQuoteEscaped("abc 'def ghi' jkl   'mno pqr' st   uv w  y z");
		assertEquals(Arrays.asList("abc", "def ghi", "jkl", "mno pqr", "st", "uv", "w", "y", "z"), parts);

		parts = StringUtil.splitOnSpaceExceptedQuoteEscaped("abc  '\"def ghi\"'  jkl 'mno \\' pqr   st uv  w y  z");
		assertEquals(Arrays.asList("abc", "\"def ghi\"", "jkl", "mno \\", "pqr", "st", "uv", "w", "y", "z"), parts);
	}
}
