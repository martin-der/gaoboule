package net.tetrakoopa.gaoboule.config;

import net.tetrakoopa.gaoboule.model.board.Composition;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class ConfigManagerTest {

	private InputStream resource(String name) {
		String resourcePath = "/net/tetrakoopa/gaoboule/config/"+name;
		InputStream stream = this.getClass().getResourceAsStream(resourcePath);
		if (stream == null) {
			throw new IllegalArgumentException("No such resource '"+resourcePath+"'");
		}
		return stream;
	}

	@Test
	public void when_config_file_is_read_CPU_object_is_built() throws IOException {
		Composition Composition = new ConfigManager().load(resource("simple-config.yaml"));
		Composition.getActions().entrySet().forEach(e -> {
			System.out.println("Action '"+e.getKey()+"'");
			e.getValue().getValues().entrySet().forEach(v-> System.out.println("  Value '"+v.getKey()+"'["+v.getValue().getMode()+"] = "+v.getValue().getExpression().replaceAll("\n[^$]", "\n        ")));
		});
	}
}
