package net.tetrakoopa.gaoboule.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

	@GetMapping("/admin")
	public String index() {
		return "Greetings from Spring Boot!";
	}

}