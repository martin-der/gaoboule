package net.tetrakoopa.gaoboule.app;

import net.tetrakoopa.gaoboule.Bot;
import net.tetrakoopa.gaoboule.identity.Identity;
import net.tetrakoopa.gaoboule.identity.IdentityManager;
import net.tetrakoopa.gaoboule.model.board.BUS;
import net.tetrakoopa.gaoboule.model.board.DefaultProcessor;
import net.tetrakoopa.gaoboule.model.board.Composition;
import net.tetrakoopa.gaoboule.config.ConfigManager;
import net.tetrakoopa.gaoboule.model.board.Processor;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.FileInputStream;

@SpringBootApplication
public class GaobouleAppApplication implements ApplicationRunner {

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(GaobouleAppApplication.class, args);
	}

	private Composition composition;
	private BUS bus;
	private Processor processor;

	private Bot bot;

	private static final String OPTION_NAME_CONFIG = "config";
	private static final String CONFIG_FILE_DEFAULT_NAME = "gaoboule";
	private static final String OPTION_NAME_IDENTITY = "identity";
	private static final String IDENTITY_FILE_DEFAULT_NAME = "identity";
	private static final String OPTION_NAME_LOG_ROOM = "log-room";

	@Override
	public void run(ApplicationArguments args) throws Exception {
//		args.getNonOptionArgs().forEach(System.out::println);

		final FileInputStream configInputStream;
		if (args.getOptionNames().contains(OPTION_NAME_CONFIG)) {
			configInputStream = new FileInputStream(args.getOptionValues(OPTION_NAME_CONFIG).get(0));
		} else {
			configInputStream = new FileInputStream("./"+CONFIG_FILE_DEFAULT_NAME+".yaml");
		}
		composition = new ConfigManager().load(configInputStream);

		bot = new Bot();
		final FileInputStream identityInputStream;
		if (args.getOptionNames().contains(OPTION_NAME_IDENTITY)) {
			identityInputStream = new FileInputStream(args.getOptionValues(OPTION_NAME_IDENTITY).get(0));
		} else {
			identityInputStream = new FileInputStream("./"+IDENTITY_FILE_DEFAULT_NAME+".yaml");
		}
		final Identity identity = new IdentityManager().load(identityInputStream);

		bus = new BUS(composition);

		processor = new DefaultProcessor(composition, bus);

		bot.start(identity, composition, bus, processor);
	}

}
